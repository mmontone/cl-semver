;;;; cl-semver.asd

(asdf:defsystem #:cl-semver
  :serial t
  :description "Describe cl-semver here"
  :author "Mariano Montone"
  :license "MIT"
  :depends-on (#:alexandria
               #:esrap
               #:cl-ppcre)
  :components ((:file "package")
               (:file "cl-semver")))

